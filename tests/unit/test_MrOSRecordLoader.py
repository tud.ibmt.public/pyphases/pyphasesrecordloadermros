from unittest import TestCase
from unittest.mock import Mock, patch

from pyPhasesRecordloader.downloader.Downloader import Downloader
from pyPhasesRecordloader.downloader.FolderDownloader import FolderDownloader
from pyPhasesRecordloaderSHHS.recordLoaders.SHHSAnnotationLoader import SHHSAnnotationLoader

from pyPhasesRecordloaderMrOS.recordLoaders.MrOSAnnotationLoader import MrOSAnnotationLoader
from pyPhasesRecordloaderMrOS.recordLoaders.RecordLoaderMrOS import RecordLoaderMrOS


class TestMrOSRecordLoader(TestCase):

    def setUp(self):
        dl = FolderDownloader({
            "basePath": "file/path"
        })
        # edf1, xml1, edf2, xml2
        dl.basePathExtensionwise = ["file/path1", "file/path2", "file/path3", "file/path4"]
        self.loader = RecordLoaderMrOS("file/path", ["signal1", "signal2"])
        self.loader.downloader = dl

    def test_AnnotationLoaderInhertitance(self):
        self.assertIsInstance(MrOSAnnotationLoader(), SHHSAnnotationLoader)

    def test_isVisit1(self):

        self.assertTrue(self.loader.isVisit1("mros-visit1-a20000"))
        self.assertFalse(self.loader.isVisit1("mros-visit2-a20000"))

    def test_getFilePathSignal(self):
        self.assertEqual(self.loader.getFilePathSignal("mros-visit1-a20000"), "file/path1/mros-visit1-a20000.edf")
        self.assertEqual(self.loader.getFilePathSignal("mros-visit2-a20000"), "file/path3/mros-visit2-a20000.edf")

    def test_getFilePathAnnotation(self):
        self.assertEqual(self.loader.getFilePathAnnotation("mros-visit1-a20000"), "file/path2/mros-visit1-a20000-nsrr.xml")
        self.assertEqual(self.loader.getFilePathAnnotation("mros-visit2-a20000"), "file/path4/mros-visit2-a20000-nsrr.xml")

    def test_groupBy(self):
        self.assertEqual(self.loader.groupBy("patient", ["mros-visit1-a20000", "mros-visit1-a20001", "mros-visit2-a20000"]), {"a20000": ["mros-visit1-a20000", "mros-visit2-a20000"], "a20001": ["mros-visit1-a20001"]})


    @patch('pyPhasesRecordloaderMrOS.recordLoaders.RecordLoaderMrOS.super')  # replace 'your_module' with the actual module name
    @patch('pyPhasesRecordloaderMrOS.recordLoaders.RecordLoaderMrOS.CSVMetaLoader')  # replace 'your_module' with the actual module name containing CSVMetaLoader
    def test_get_meta_data(self, MockCSVMetaLoader, MockSuper):

        # Arrange
        instance = self.loader
        recordName = "some_record_name"
        super_meta_data = {"key1": "value1"}
        csv_meta_data = {"key2": "value2"}
        
        MockSuper().getMetaData.return_value = super_meta_data
        instance.isVisit1 = Mock(return_value=True)  # assuming isVisit1 is a method of the instance

        mock_csv_loader = Mock()
        mock_csv_loader.getMetaData.return_value = csv_meta_data
        MockCSVMetaLoader.return_value = mock_csv_loader

        expected_meta_data = {
            "recordId": recordName,
            "key1": "value1",
            "key2": "value2",
        }

        # Act
        result = instance.getMetaData(recordName)

        # Assert
        self.assertEqual(result, expected_meta_data)