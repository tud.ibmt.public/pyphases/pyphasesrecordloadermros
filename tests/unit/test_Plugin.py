from unittest import TestCase

from pyPhases import ConfigNotFoundException, Project, pdict

from pyPhasesRecordloaderMrOS.Plugin import Plugin
from pyPhasesRecordloader import RecordLoader


class TestPlugin(TestCase):
    def test_loaderMisconfigured(self):
        project = Project()
        project.config = pdict({})
        plugin = Plugin(project)

        # useLoader not specified
        self.assertRaises(ConfigNotFoundException, plugin.initPlugin)
        self.project.config = pdict({"useLoader": "mros"})
        plugin = Plugin(self.project)
        self.assertRaises(ConfigNotFoundException, plugin.initPlugin)

    def setUp(self):
        self.options = {}
        self.project = Project()
        self.project.addPlugin("pyPhasesRecordloaderMrOS", self.options)
        self.plugin = self.project.plugins[-1]
        self.project.config.update({"useLoader": "mros", "mros-path": "./data"})

    def test_initPlugin(self):

        self.plugin.initPlugin()

        self.assertIn("RecordLoaderMrOS", RecordLoader.recordLoaders)
        self.assertIn("MrOSAnnotationLoader", RecordLoader.recordLoaders)
        self.assertIn("mros", self.project.config["loader"])

        self.assertEqual(self.project.config["loader"]["mros"]["dataBase"], "mros")
        self.assertEqual(
            self.project.config["loader"]["mros"]["dataset"]["downloader"]["basePath"],
            "data",
        )
        self.assertEqual(
            self.project.getConfig("loader.mros.dataset.downloader.basePathExtensionwise")[0],
            "data/polysomnography/edfs/visit1",
        )
